package com.zuitt;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/details")

public class DetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DetailsServlet() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  // Retrieve the data using the methods you specified
        ServletConfig config = getServletConfig();
        
        String firstName = System.getProperty("firstName");
        String lastName = (String) request.getSession().getAttribute("lastName");
        String email = (String) config.getServletContext().getAttribute("email");
        String contactNumber = request.getParameter("contactNumber");

        // Send the data as a response
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>User Details</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1> Welcome to PhoneBook </h1>");
        out.println("<p>First Name: " + firstName + "</p>");
        out.println("<p>Last Name: " + lastName + "</p>");
        out.println("<p>Email: " + email + "</p>");
        out.println("<p>Contact Number: " + contactNumber + "</p>");
        out.println("</body>");
        out.println("</html>");
	}

}
