package com.zuitt;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/user")

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UserServlet() {
        super();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("index.html");
        
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String contactNumber = request.getParameter("contactNumber");
        
        // Set the first name as a system property
        System.setProperty("firstName", firstName);

        // Store the last name in the session
        HttpSession session = request.getSession();
        session.setAttribute("lastName", lastName);

        // Store the email in the servlet context
        ServletContext context = getServletContext();
        context.setAttribute("email", email);

        // Use URL rewriting to pass the contact number to another servlet
        response.sendRedirect("details?contactNumber=" + contactNumber);
		
		
		
	}

}
